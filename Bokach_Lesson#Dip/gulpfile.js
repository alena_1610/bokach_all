const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const autoprefixer = require('gulp-autoprefixer');

var imageTask = function(){
  gulp.src('./images/*')
      .pipe(image())
      .pipe(gulp.dest('./build/images'))
};
var sassTask = function () {
    return gulp.src('./styles/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
          browsers: ['last 2 versions'],
          cascade: false
        }))
        .pipe(gulp.dest('./build/styles/'));
};
var fontsTask = function () {
    return gulp.src('./fonts/*')
        .pipe(gulp.dest('./build/fonts'));
};

gulp.task('image', imageTask);
gulp.task('sass', sassTask);
gulp.task('fonts', fontsTask);

var watchTask = function () {
    gulp.watch('styles/*.scss', ['sass']);
    gulp.watch('images/*', ['image']);
    gulp.watch('fonts/*', ['fonts']);
};

gulp.task('watch', ['sass', 'image', 'fonts'], watchTask);

gulp.task('default', ['watch', 'fonts', 'image']);
