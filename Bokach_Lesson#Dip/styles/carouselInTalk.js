
/*первый блок*/
function openbox1(carousel__item1) {
  display = document.getElementById('carousel__item1').style.display;
  carousel__item1 = carousel__item1 || 'не выбран'; /*дефолтное значение*/
  carousel__item5 = carousel__item5 || 'не выбран'; /*дефолтное значение*/
  if (display == "none") {
    document.getElementById('carousel__item1').style.display = "block";
    document.getElementById('carousel__item5').style.display = "block";
    document.getElementById('carousel__item2').style.display = "none";
    document.getElementById('carousel__item3').style.display = "none";
    document.getElementById('carousel__item4').style.display = "none";
    document.getElementById('carousel__item6').style.display = "none";
    document.getElementById('carousel__item7').style.display = "none";
    document.getElementById('carousel__item8').style.display = "none";
  }
  else {
    document.getElementById('carousel__item1').style.display = "block";
    document.getElementById('carousel__item5').style.display = "block";
  }
}

/*2 блок*/
function openbox2(carousel__item2) {
  display = document.getElementById('carousel__item2').style.display;
  if (display == "none") {
    document.getElementById('carousel__item2').style.display = "block";
    document.getElementById('carousel__item6').style.display = "block";
    document.getElementById('carousel__item1').style.display = "none";
    document.getElementById('carousel__item3').style.display = "none";
    document.getElementById('carousel__item4').style.display = "none";
    document.getElementById('carousel__item5').style.display = "none";
    document.getElementById('carousel__item7').style.display = "none";
    document.getElementById('carousel__item8').style.display = "none";
}
  else {
    document.getElementById('carousel__item2').style.display = "none";
    document.getElementById('carousel__item6').style.display = "none";
  }
}
/*3 блок*/
function openbox3(carousel__item3) {
  display = document.getElementById('carousel__item3').style.display;
  if (display == "none") {
    document.getElementById('carousel__item3').style.display = "block";
    document.getElementById('carousel__item7').style.display = "block";
    document.getElementById('carousel__item1').style.display = "none";
    document.getElementById('carousel__item2').style.display = "none";
    document.getElementById('carousel__item4').style.display = "none";
    document.getElementById('carousel__item5').style.display = "none";
    document.getElementById('carousel__item6').style.display = "none";
    document.getElementById('carousel__item8').style.display = "none";
  }
  else {
    document.getElementById('carousel__item3').style.display = "none";
    document.getElementById('carousel__item7').style.display = "none";
  }
}
/*4 блок*/
function openbox4(carousel__item4) {
  display = document.getElementById('carousel__item4').style.display;
  if (display == "none") {
    document.getElementById('carousel__item4').style.display = "block";
    document.getElementById('carousel__item8').style.display = "block";
    document.getElementById('carousel__item1').style.display = "none";
    document.getElementById('carousel__item2').style.display = "none";
    document.getElementById('carousel__item3').style.display = "none";
    document.getElementById('carousel__item5').style.display = "none";
    document.getElementById('carousel__item6').style.display = "none";
    document.getElementById('carousel__item7').style.display = "none";
  }
  else {
    document.getElementById('carousel__item4').style.display = "none";
    document.getElementById('carousel__item8').style.display = "none";
  }
}

var slideIndex = 1;
openbox(slideIndex);

function openbox(n) {
  var i;
  var slides = document.getElementsByClassName("carousel__item");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
